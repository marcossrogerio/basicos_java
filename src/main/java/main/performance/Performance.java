package main.performance;

import java.util.ArrayList;
import java.util.LinkedList;

public class Performance {
    long startTime, endTime, duration;

    ArrayList<Integer> arrayList = new ArrayList<>();
    LinkedList<Integer> linkedList = new LinkedList<>();

    public void insereElementoArrayList() {
        for (int i = 0; i < 1000000; i++) {
            arrayList.add(i);
        }
    }

    public void insereElementoLinkedList() {
        for (int i = 0; i < 1000000; i++) {
            linkedList.add(i);
        }
    }

    public void adicionaArrayList() {
        insereElementoArrayList();
        startTime = System.nanoTime();
        arrayList.indexOf(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de busca do Index: " + duration);
    }

    public void adicionaLinkedList() {
        insereElementoLinkedList();
        startTime = System.nanoTime();
        linkedList.indexOf(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de busca do Index: " + duration);
    }

    public void removeArrayList() {
        startTime = System.nanoTime();
        arrayList.remove(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de remoção do Index: " + duration);
    }

    public void removeLinkedList() {
        startTime = System.nanoTime();
        linkedList.remove(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de remoção do Index: " + duration);
    }
}