package main.basico_java3.equals_e_hashcode;

public class Test {
    public static void main(String[] args) {

        Pessoa pessoa1 = new Pessoa("João", 35);
        Pessoa pessoa2 = new Pessoa("Marcos", 35);

        System.out.println(pessoa1.equals(pessoa2));

    }
}
