package main.basico_java3.equals_e_hashcode;

import java.util.Objects;

//Reflexivo: o1.equals (o1) - o que significa que um objeto (por exemplo, o1) deve ser igual a si mesmo
//Simétrico: o1.equals (o2) se e somente o2.equals (o1)
//Transitivo: o1.equals (o2) && o2.equals (o3) implica que o1.equals (o3) também
//Consistente: o1.equals (o2) retorna o mesmo, desde que o1 e o2 não sejam modificados

public class Pessoa {
    private String nome;
    private int idade;

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public Pessoa(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pessoa pessoa = (Pessoa) o;
        return Objects.equals(nome, pessoa.nome) && Objects.equals(idade, pessoa.idade);
    }

}
