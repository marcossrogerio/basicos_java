package main.basico_java3.threads_multithreads.threads3;

public class Synchronized {
    static int i = -1;

    public static void main(String[] args) {
        MeuRunnable runnable = new MeuRunnable();
        Thread t0 = new Thread(runnable);
        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);
        Thread t3 = new Thread(runnable);
        Thread t4 = new Thread(runnable);

        t0.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }

    public static class MeuRunnable implements Runnable {
        static final Object lock1 = new Object();
        static final Object lock2 = new Object();

        @Override
        public void run() {
            synchronized (lock1) {
                i++;
                String nome = Thread.currentThread().getName();
                System.out.println(nome + ":" + i);
            }
            synchronized (lock2){
                i++;
                String nome = Thread.currentThread().getName();
                System.out.println(nome + ":" + i);
            }
        }
    }
}