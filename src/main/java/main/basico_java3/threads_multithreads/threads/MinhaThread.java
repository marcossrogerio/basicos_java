package main.basico_java3.threads_multithreads.threads;

public class MinhaThread extends Thread {
    private String nome;
    private int tempo;

    public MinhaThread(String nome, int tempo) {
        this.nome = nome;
        this.tempo = tempo;
        start(); // Iniciando a Thread
    }


    public void run() {

       for (int i = 0; i < 6; i++) {
            System.out.println(nome + "contador " + i);
            try {
                Thread.sleep(tempo); // Adiciona tempo na execução da Thread
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}