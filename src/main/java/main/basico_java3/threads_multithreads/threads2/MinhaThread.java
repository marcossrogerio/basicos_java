package main.basico_java3.threads_multithreads.threads2;

public class MinhaThread implements Runnable {

    private String nome;
    private int tempo;

    public MinhaThread(String nome, int tempo) {
        this.nome = nome;
        this.tempo = tempo;
    }

    @Override
    public void run() {

        for (int i = 0; i < 6; i++) {
            System.out.println(nome + "contador " + i);

            try {
                Thread.sleep(tempo);
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
        }
    }
}

