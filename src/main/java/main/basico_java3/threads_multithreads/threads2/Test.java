package main.basico_java3.threads_multithreads.threads2;

public class Test {
    public static void main(String[] args) {

        MinhaThread thread = new MinhaThread("#1", 600);
        MinhaThread thread1 = new MinhaThread("#2", 200);
        Thread t1 = new Thread(thread);
        Thread t2 = new Thread(thread1);
        t1.start();
        t2.start();
    }
}
