package main.basico_java3.estrutura_de_dados.sets;

import java.util.HashSet;
import java.util.Set;

public class TestSet {
    public static void main(String[] args) {
        Set<String> colecaoSet = new HashSet<>();
        colecaoSet.add("Site");
        colecaoSet.add("Blog");
        colecaoSet.add("Página");
        colecaoSet.add("Blog");

        System.out.println("Tamanho da coleção: " + colecaoSet.size());
        int count = 0;
        for (String valor : colecaoSet) {
            System.out.println(++count + " = " + valor);
        }
    }

}
