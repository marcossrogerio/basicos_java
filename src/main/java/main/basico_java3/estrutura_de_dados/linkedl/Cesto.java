package main.basico_java3.estrutura_de_dados.linkedl;

import java.util.List;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Cesto {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>(List.of("maçã", "uva", "banana"));//cria uma nova lista encadeada
        linkedList.push("laranja");//adiciona ao início
        String headElement = linkedList.poll();//remove o elemento da frente
        System.out.println("Removido: " + headElement);//imprime "Removido: laranja"

        String collect = linkedList.stream().sorted().collect(Collectors.joining(", "));

        linkedList.clear();

        System.out.println(collect);
    }
}
