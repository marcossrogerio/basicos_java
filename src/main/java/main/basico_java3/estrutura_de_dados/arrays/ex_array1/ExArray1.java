package main.basico_java3.estrutura_de_dados.arrays.ex_array1;

import java.util.Arrays;

public class ExArray1 {
    public static void main(String[] args) {
        int[] arrayInteiros = new int[5];
        int[] numeros = {2, 3, 1, 5, 4};
        System.out.println(numeros.length);
        System.out.println(numeros[0]);
        Arrays.sort(numeros);
        System.out.println(Arrays.toString(numeros));
    }
}
