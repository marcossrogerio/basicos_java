package main.basico_java3.estrutura_de_dados.arrays.ex_array2;

public class Vetor {

    private String[] elementos;
    private int tamanho;

    public Vetor(int capacidade) {
        this.elementos = new String[capacidade];
        this.tamanho = 0;
    }

    public void adiciona(String elemento) throws Exception { // Método que adiciona elementos dentro do Array
        if (this.tamanho < this.elementos.length) {
            this.elementos[this.tamanho] = elemento;
            this.tamanho++;
        } else {
            throw new Exception("Não é possível adicionar mais elementos");
        }
    }

    public String busca(int posicao) { // Busca pela posição do elemento no Array.
        if (!(posicao >= 0 && posicao < tamanho)){
            throw new IllegalArgumentException("Posição Inválida");
        }else
            return this.elementos[posicao];
    }

    @Override
    public String toString() { // Utilizando StringBuilder. Exibe as posições preenchidas do Array, ignorando as vazias.
        StringBuilder s = new StringBuilder();
        s.append("[");

        for (int i = 0; i < this.tamanho - 1; i++) {
            s.append(this.elementos[i]);
            s.append(", ");
        }

        if (this.tamanho > 0) {
            s.append(this.elementos[this.tamanho - 1]);
        }
        s.append("]");

        return s.toString();
    }

    public int tamanho() {
        return this.tamanho;


    }
}
