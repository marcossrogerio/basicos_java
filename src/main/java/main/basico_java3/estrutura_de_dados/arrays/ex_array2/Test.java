package main.basico_java3.estrutura_de_dados.arrays.ex_array2;

public class Test {
    public static void main(String[] args) {

        try {
            Vetor vetor = new Vetor(10);
            vetor.adiciona("elemento 1");
            vetor.adiciona("elemento 2");
            vetor.adiciona("elemento 3");
            vetor.adiciona("elemento 4");
            vetor.adiciona("elemento 5");
            vetor.adiciona("elemento 6");

            System.out.println(vetor.tamanho());

            System.out.println(vetor);

            System.out.println(vetor.busca(5));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
