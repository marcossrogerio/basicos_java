package main.basico_java3.estrutura_de_dados.stacks;

import java.util.Random;
import java.util.Stack;

public class Pilha {
    public static void main(String[] args) {
        Stack pilha = new Stack();
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            System.out.println("Inserindo na pilha: " + pilha.push(random.nextInt(50)));
            System.out.println(pilha);
        }
        System.out.println('\n');

        for (int i = 0; i < 10; i++) {
            System.out.println("Retirando da pilha: " + pilha.pop());
            System.out.println(pilha);
        }
    }
}
