package main.basico_java.parte2;

public class Gato implements Mamifero, Animal {

    private String raca;

    public Gato(String raca) {
        this.raca = raca;
    }


    public String nome() {
        return "Lili";
    }

    @Override
    public String emitirSom() {
        return null;
    }


    public boolean amamentar() {

        return true;
    }

    public void printGato(){
        System.out.println(raca + '\n' + nome() + '\n' + emitirSom() + '\n' + amamentar() + '\n');
    }
}
