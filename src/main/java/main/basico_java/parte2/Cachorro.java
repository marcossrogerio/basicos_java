package main.basico_java.parte2;

public class Cachorro implements Mamifero, Animal {

    private final String tamanho;
    private final String raca;

    public Cachorro(String tamanho, String raca) {
        this.tamanho = tamanho;
        this.raca = raca;
    }

    public void printCachorro() {
        System.out.println(tamanho + '\n' + raca + '\n' + nome() + '\n' + emitirSom() + '\n' + amamentar() + '\n');
    }


    @Override
    public String nome() {
        return null;
    }

    @Override
    public String emitirSom() {
        return null;
    }

    @Override
    public boolean amamentar() {
        return false;
    }
}

