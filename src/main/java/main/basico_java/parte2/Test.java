package main.basico_java.parte2;

public class Test {
    public static void main(String[] args) {

        Cachorro cachorro = new Cachorro("Pequeno", "Pinscher");
        Gato gato = new Gato("Snowshoe");
        Papagaio papagaio = new Papagaio();


        cachorro.printCachorro();
        gato.printGato();
        papagaio.printPapagaio();
    }
}
