// Interface; Polimorfismo

package main.basico_java.parte2;

public interface Animal {
    String nome();

    String emitirSom();
}

