// Controle de Acesso; Final; Static;

package main.basico_java.parte3;

public class Lampada2 extends Lampada {
    private boolean acesa;
    private int potenciaDaLampada;

    protected Lampada2(Boolean acesa, double preco) {
        super();
        this.acesa = acesa;
        Lampada.preco = 22;

    }

    protected Lampada2(boolean acesa, int potenciaDaLampada, String cor) {
        super();
        this.acesa = acesa;
        this.potenciaDaLampada = potenciaDaLampada;
    }


    protected void informarSituacaoA(Lampada2 lampada) {
        if (acesa) {
            System.out.println("A lâmpada está acesa");
        } else {
            System.out.println("A lãmpada não está acesa");
        }
    }

    protected void informarPotenciaA(Lampada2 lampada) {
        System.out.println("Potência " + potenciaDaLampada + '\n' + "cor = " + getCor() + '\n' + Lampada.preco + '\n');

    }

    protected void informarSituacaoB(Lampada2 lampada) {
        if (!acesa)
            System.out.println("A lâmpada não está acesa");
        else {
            System.out.println("A lâmpada está acesa");
        }

    }

    protected void informarPotenciaB(Lampada2 lampada2) {
        System.out.println("Potência " + potenciaDaLampada);

    }

}