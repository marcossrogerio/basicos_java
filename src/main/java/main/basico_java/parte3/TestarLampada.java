package main.basico_java.parte3;

public class TestarLampada {
    public static void main(String[] args) {
        Lampada2 lampadaLab1 = new Lampada2(true, 22);

        Lampada2 lampadalab2 = new Lampada2(false, 20, "Verde");



        lampadaLab1.informarSituacaoA(lampadaLab1);
        lampadaLab1.informarPotenciaA(lampadaLab1);

        lampadalab2.informarSituacaoB(lampadalab2);
        lampadalab2.informarPotenciaB(lampadalab2);



    }

}
