package main.teste.teste3;

import java.util.ArrayList;
import java.util.LinkedList;

public class Teste {
    public static void main(String[] args) {
        long startTime, endTime, duration;

        ArrayList<Integer> arrayList = new ArrayList<>(1000000);
        LinkedList<Integer> linkedList = new LinkedList<>();


        for (int i = 0; i < 1000000; i++) {;
            arrayList.add(i);
            linkedList.add(i);
        }

        System.out.println("Tamanho do ArrayList: " + arrayList.size());
        System.out.println("Tamanho do ArrayList: " + linkedList.size() + '\n');

        startTime = System.nanoTime();
        arrayList.indexOf(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de busca do Index: " + duration);

        startTime = System.nanoTime();
        linkedList.indexOf(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de busca do Index: " + duration);

        startTime = System.nanoTime();
        linkedList.addFirst(300000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de adição no início do Index: " + duration);

        startTime = System.nanoTime();
        arrayList.add(0,300000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de adição no início do Index: " + duration);

        startTime = System.nanoTime();
        linkedList.add(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de adição no meio do Index: " + duration);

        startTime = System.nanoTime();
        arrayList.add(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de adição no meio do Index: " + duration);

        startTime = System.nanoTime();
        linkedList.addLast(400000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de adição no final do Index: " + duration);

        startTime = System.nanoTime();
        arrayList.add(1000000, 400000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de adição no final do Index: " + duration);

        startTime = System.nanoTime();
        arrayList.remove(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de remoção do Index: " + duration);

        startTime = System.nanoTime();
        linkedList.remove(500000);
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de remoção do Index: " + duration);

        startTime = System.nanoTime();
        linkedList.clear();
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de limpeza do Index: " + duration);

        startTime = System.nanoTime();
        arrayList.clear();
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Tempo de limpeza do Index: " + duration);

        System.out.println(arrayList);
        System.out.println(linkedList);
















    }
}