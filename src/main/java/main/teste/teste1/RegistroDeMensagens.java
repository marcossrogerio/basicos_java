package main.teste.teste1;

import java.util.*;

public class RegistroDeMensagens {
    public static void main(String[] args) throws InterruptedException {
        Collection<String> mensagens = new ArrayList<>();

        Thread thread1 = new Thread(new ProduzMensagem(0,10000, mensagens));
        Thread thread2 = new Thread(new ProduzMensagem(10000,20000, mensagens));
        Thread thread3 = new Thread(new ProduzMensagem(20000,30000, mensagens));

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        System.out.println("Threads produtoras de mensagens finalizadas");

        for(int i = 0; i < 15000; i++){
            if(!mensagens.contains("Mensagem " + i)){
                throw new IllegalStateException("Não encontrei a mensagem!");
            }
        }
        if(mensagens.contains(null)){
            throw new IllegalStateException("Não devia ter null aqui dentro");
        }
        System.out.println("Fim da execução");
    }
}
