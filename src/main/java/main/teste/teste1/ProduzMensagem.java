package main.teste.teste1;

import java.util.Collection;

public class ProduzMensagem implements Runnable {

    private int comeco;
    private int fim;
    private Collection mensagens;

    public ProduzMensagem(int comeco, int fim, Collection mensagens) {
        this.comeco = comeco;
        this.fim = fim;
        this.mensagens = mensagens;
    }

    @Override
    public void run() {
        for (int i = comeco; i < fim; i++) {
            synchronized (mensagens) {
                mensagens.add("Mensagem " + i);
            }
        }
    }
}
