package main.basico_java2.acesso_referencia_e_copia;

public class ContaBancaria {

    private double saldo;
    private String nome;

    public void nomeCliente(String nome){
        this.nome = nome;
    }

    public boolean depositar(Double valor) {
        Double valorDeposito = valor;
        valor = 350D;
        System.out.println(valor);
        if (valorDeposito > 0) {
            this.saldo = saldo + valorDeposito;
            return true;
        } else {
            return false;
        }

    }

    public boolean sacar(double valorSaque) {
        if (this.saldo < valorSaque) {
            return false;
        } else {
            this.saldo = saldo - valorSaque;
            return true;
        }
    }

    public void pegarNomeDoCliente(){
        System.out.println("Nome do cliente: " + nome);
    }

    public void extrato(){
        System.out.printf("Valor atual da conta: R$%.2f \n", saldo);
    }
}
