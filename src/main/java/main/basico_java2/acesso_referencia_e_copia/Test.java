package main.basico_java2.acesso_referencia_e_copia;

public class Test {
    public static void main(String[] args) {

        ContaBancaria contaBancaria = new ContaBancaria();

        contaBancaria.nomeCliente("Marcos");
        boolean retornoDeposita = contaBancaria.depositar(100D); //

        contaBancaria.pegarNomeDoCliente();


        if(retornoDeposita){
            contaBancaria.extrato();
        }
        else {
            System.out.println("O valor do depósito é inválido!");
        }

        boolean retornoSaque = contaBancaria.sacar(75);
        if(!retornoSaque){
            System.out.println("O valor do saque é maio que o valor da conta!");
        }
        else {
            contaBancaria.extrato();
        }
    }
}