package main.basico_java2.trycatch;

public class TryCatchMultiplos {
    public static void main(String[] args) {


        int[] numeros = {4, 8, 16, 32, 64, 128};
        int[] demons = {2, 0, 4, 8, 0};

            for (int i = 0; i < numeros.length; i++) {
                try {
                    System.out.println(numeros[i] + "/" + demons[i] + " = " + (numeros[i] / demons[i]));
                }catch (ArithmeticException er1){
                    System.err.println("Erro ao dividir por 0");
                }catch (ArrayIndexOutOfBoundsException er1){
                    System.err.println("Posição inválido do Array");
                }
        }
    }
}


