package main.basico_java2.trycatch;


public class TryCatch {
    public static void main(String[] args) {


        try {
            int[] vetor = new int[4];

            System.out.println("Antes da execução");

            vetor[5] = 10;

            System.out.println("Este texto não será empresso");
        } catch (ArrayIndexOutOfBoundsException exception){
            System.out.println("Este texto será impresso");
        }

    }
}
