package main.basico_java2.throwethrows;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExThrows {
    public static void main(String[] args) throws InputMismatchException {



        System.out.println("Entre com um número decimal");

        leNumero();
    }

        public static int leNumero() throws InputMismatchException {
            Integer numero = 0;
            Scanner scanner = new Scanner(System.in);
            numero = scanner.nextInt();
            if (numero.getClass() == Integer.class) {
                return numero;
            }else throw new InputMismatchException("Não foi digitado um número");
        }
}
