package main.basico_java2.enums;

public class TesteEnum {
    public static void main(String[] args) {

        DiaSemana dia = DiaSemana.DOMINGO;

        System.out.println(dia + " - " + dia.getValor() + '\n');

        DiaSemana[] dias = DiaSemana.values(); //Utilizando "values"

        for(int i = 0; i<dias.length; i++){
            System.out.println(dias[i]);
        }

        DiaSemana diaas;

        System.out.println(Enum.valueOf(DiaSemana.class, "DOMINGO")); //Utilizando "valueof"
    }

    Data data = new Data(1, 4, 2021, DiaSemana.SEXTA);

}
